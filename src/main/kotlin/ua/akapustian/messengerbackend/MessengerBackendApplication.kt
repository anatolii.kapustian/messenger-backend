package ua.akapustian.messengerbackend

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.data.jpa.repository.config.EnableJpaRepositories

@EnableJpaRepositories
@SpringBootApplication
class MessengerBackendApplication

fun main(args: Array<String>) {
    runApplication<MessengerBackendApplication>(*args)
}
