package ua.akapustian.messengerbackend.service.impl

import org.springframework.stereotype.Service
import ua.akapustian.messengerbackend.domain.model.Chat
import ua.akapustian.messengerbackend.domain.repository.ChatRepository
import ua.akapustian.messengerbackend.service.ChatService

@Service
class ChatServiceImpl (
        val chatRepository: ChatRepository
): ChatService {

    override fun findByName(name: String): Chat =
            chatRepository.findByName(name) ?: throw RuntimeException("Unknown error")

    override fun findAll(): Set<Chat> =
            chatRepository.findAll()
                .toSet()
}