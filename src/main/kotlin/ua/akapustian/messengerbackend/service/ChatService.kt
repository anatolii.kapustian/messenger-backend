package ua.akapustian.messengerbackend.service

import ua.akapustian.messengerbackend.domain.model.Chat

interface ChatService {

    fun findAll(): Set<Chat>

    fun findByName(name: String): Chat
}