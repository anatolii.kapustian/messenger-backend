package ua.akapustian.messengerbackend.domain.model

import javax.persistence.*

@Entity
@Table(name = "chat")
class Chat(

        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        var id: Int,

        @Column
        var name: String
)