package ua.akapustian.messengerbackend.domain.repository

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import ua.akapustian.messengerbackend.domain.model.Chat
import java.util.*

@Repository
interface ChatRepository : CrudRepository<Chat, Int> {

    fun findByName(name: String): Chat?
}