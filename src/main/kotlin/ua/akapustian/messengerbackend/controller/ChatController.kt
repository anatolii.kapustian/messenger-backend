package ua.akapustian.messengerbackend.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import ua.akapustian.messengerbackend.domain.model.Chat
import ua.akapustian.messengerbackend.service.ChatService

@RestController
@RequestMapping("/api")
class ChatController @Autowired constructor(
        val chatService: ChatService
) {

    @GetMapping
    fun findAll(): Set<Chat> = chatService.findAll()

    @GetMapping("/search")
    fun search(name: String): Chat = chatService.findByName(name)
}